from csv import reader, writer
import sys
import re
import math
import importlib.machinery

custom_module = None

if len(sys.argv) == 4:
    file_name = sys.argv[3]
    loader = importlib.machinery.SourceFileLoader(file_name, file_name)
    lib = loader.load_module(file_name)


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def get_math_funcs():
    li2 = []
    li1 = [x+'\(' for x in dir(math) if not x.startswith('__')]
    if custom_module:
        li2 = [y+'\(' for y in dir(lib) if not y.startswith('__')]
    return li1+li2


def math_replace(table, i):
    li = []
    collection = re.finditer('(' + '|'.join(get_math_funcs()) + ')', table[i])
    for obj in collection:
        if obj.group() not in li:
            li.append(obj.group())

    for mat_p in li:
        if hasattr(math, mat_p[0:-1]):
            prefix = 'math.'
        elif hasattr(lib, mat_p[0:-1]):
            prefix = 'lib.'
        table[i] = table[i].replace(mat_p, prefix + mat_p)


def do_references(table, i):
    where = re.finditer('(' + '|'.join(list(table.keys())) + ')', table[i])
    for r in where:
        try:
            if not is_number(table[r.group()]):
                table[i] = table[i].replace(r.group(),
                                            '\"'+str(table[r.group()])+'\"')
            else:
                table[i] = table[i].replace(r.group(), str(table[r.group()]))
        except KeyError:
            table[i] = "ERROR"


def create_dict():
    table = {}
    with open(sys.argv[1], 'r') as csv_file:  # auto closes the file anyway
        file_reader = reader(csv_file, dialect='excel', skipinitialspace=True)

        row_num = 0
        for row in file_reader:
            col_name = 65  # ASCII value of 'A'
            row_num += 1
            for i in row:
                table[chr(col_name) + str(row_num)] = i
                col_name += 1
    return col_name - 65, table


def complete(table):
    for i in table:
        try:
            if table[i][0] == '=':
                table[i] = eval(table[i][1:])
            elif isinstance(table[i], str):
                pass
            else:
                table[i] = eval(table[i])
        except:
            table[i] = "ERROR"


def calculate(table):
    statement = re.compile("^[^=]*$")
    st_gen = (x for x in table if table[x][0] not in ['=', '\"'])

    for i in st_gen:
        st_pat = statement.match(table[i])
        if st_pat is not None:
            # leave it as correct pattern matching
            pass
        else:
            table[i] = "ERROR"

    # when all data is passed, start (try -- in func)
    # to evaluate corresponding formulas

    formula = re.compile("^=(.)*$")
    f_gen = (x for x in table if table[x][0] is '=')

    for i in f_gen:
        if table[i][1] == table[i][-1] == '\'':
            table[i] = '=' + table[i][2:-1]
        do_references(table, i)

    f_gen_repl = (x for x in table if table[x][0] is '=')
    for j in f_gen_repl:
        f_pat = formula.match(table[j])
        if f_pat is not None:
            math_replace(table, j)
        else:
            table[j] = "ERROR"

    complete(table)


def write_in_file(table, filename, p):
    with open(filename, 'w') as output_file:
        file_writer = writer(output_file, delimiter=',')
        # sorting by rows-then-col-names to print into line
        row = [table[k] for k in
               sorted(table, key=lambda k: (int(k[1:]), k[0]))]

        # output with formatted string precision
        for i in range(0, len(row)-1, p):
            to_output = [
                            format(float(x), '.5g')
                            if not isinstance(x, str)
                            else x
                            for x in row[i:i+p]
                        ]
            file_writer.writerow(to_output)

if __name__ == "__main__":
    if len(sys.argv) == 4:
        try:
            custom_module = True
        except ImportError:
            print("Wrong importing module name is set!")
    elif len(sys.argv) > 4 or len(sys.argv) < 2:
        print("Invalid number of command line arguments!")
        exit(-1)

    (parts, excel) = create_dict()
    calculate(excel)
    write_in_file(excel, sys.argv[2], parts)
