extern "C" {
    #include <Python.h>
    #include <lua.h>
    #include <lualib.h>
    #include <lauxlib.h>

}

#include <string>
#define BUFF_SIZE 1024

static PyObject* lua_eval(PyObject* self, PyObject* args)
{
    char* buff = (char*)malloc(sizeof(char)*BUFF_SIZE);
    const char* command;
    int sts;

    if (!PyArg_ParseTuple(args,"s",&command))
        return NULL;

    std::string python_command(command);
    std::string lua_command = "loadstring(print(\'" + python_command + "\')";
    const char* lc = lua_command.c_str();

    lua_State* L = luaL_newstate();
    luaL_openlibs(L);
    luaL_loadbuffer(L,buff,strlen(buff),lc);
    int res = lua_pcall(L,0,0,0);
    lua_close(L);
    free(buff);

    return PyLong_FromLong(res);
}


static PyMethodDef LuaMethods[] = {
        {"eval", lua_eval, METH_VARARGS, "Evaluate via lua interpreter"},
        {NULL, NULL, 0, NULL}       //sentinel
};

static struct PyModuleDef luamodule = {
        PyModuleDef_HEAD_INIT,
        "lua",
        NULL,       // module documentation
        -1,         //
        LuaMethods
};

PyMODINIT_FUNC PyInit_lua(void)
{
    return PyModule_Create(&luamodule);
}