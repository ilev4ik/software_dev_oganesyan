%%%-------------------------------------------------------------------
%%% @author levon
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 24. Май 2016 4:16
%%%-------------------------------------------------------------------
-module(atm_sup).
-author("levon").

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

start_link() ->
  supervisor:start_link({local, ?SERVER}, ?MODULE, []).

init([]) ->
  {ok, {{rest_for_one, 1000, 50},
    [
      {trans_server,
      {atm_transactions, start_link, []},
      permanent, 2000, worker, [atm_transactions]},

      {atm_server,
      {atm_server, start_link, []},
      permanent, 2000, worker, [atm_server]}
    ]
  }}.

