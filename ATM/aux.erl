-module(aux).
-author("levon").

%% API
-export([concat/2]).

%% Auxiliary function
concat(L,[]) ->
  L;
concat(L,[H|T]) ->
  concat(L ++ [H],T).
