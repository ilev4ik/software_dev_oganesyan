-module(atm_server).
-author("levon").
-include("server_addr.hrl").
-include("processing_addr.hrl").
-behaviour(gen_server).

%% API
-export([start_link/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).


%%%===================================================================
%%% API
%%%===================================================================


%% Starts the server
start_link() ->
  gen_server:start_link(?SERVER, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%% Initializes the server
init([]) ->
  {ok, ?INIT_ATM_STATE}.

handle_call({widthdraw, Amount}, _, State) ->
  {_state, Result, NewState} =
  try atm:widthdraw(Amount, State) of
    ATM -> ATM
  catch
      _:error -> exit(normal)
  end,

  case _state of
    ok -> gen_server:cast(?PROCESS, {widthdraw, Amount});
    request_another_amount -> ok
  end,
  {reply, {_state, Result}, NewState}.

handle_cast(terminate, _State) ->
  exit(normal),
  {noreply, _State}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.
