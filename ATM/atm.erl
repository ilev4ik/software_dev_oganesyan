-module(atm).
-author("levon").

%% imports
-import(lists, [sort/1, reverse/1]).
-import(aux, [concat/2]).

%% API
-export([widthdraw/2]).

widthdraw(Amount, Banknotes) ->
  if
    Banknotes == [] -> throw(error);
    true -> ok
  end,
widthdraw(Amount, lists:reverse(lists:sort(Banknotes)), [], Banknotes, Banknotes).

widthdraw(0, [], Result, Rest, _) ->
  {ok, Result, Rest};
widthdraw(_, [], _, _, PrevState) ->
  {request_another_amount, [], PrevState};
widthdraw(Amount, [Head|Tail], Result, Rest, Banknotes) ->
  if
    Amount >= Head -> widthdraw(Amount-Head, Tail, concat(Result, [Head]), Rest -- [Head], Banknotes);
    Amount < Head -> widthdraw(Amount, Tail, Result, Rest, Banknotes)
  end.