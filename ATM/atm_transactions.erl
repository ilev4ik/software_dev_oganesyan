-module(atm_transactions).
-author("levon").
-include("processing_addr.hrl").
-behaviour(gen_server).
-import(aux, [concat/2]).

%% API
-export([start_link/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).


start_link() ->
  gen_server:start_link(?PROCESS, ?MODULE, [], []).

init([]) ->
  {ok, [], 10}.

handle_call(history, _, State) ->
  {reply, State, State}.

handle_cast({widthdraw, Amount}, State) ->
  {noreply, concat(State, [Amount])};
handle_cast(clear, _) ->
  {noreply, []}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.
